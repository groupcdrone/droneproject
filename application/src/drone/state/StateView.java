/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.state;

import drone.View;

/**
 *
 * @author Mike
 */
public interface StateView extends View {
    public void setFlyingState(boolean flying);
    public void setSpeed(int speed);
    public void setAltitude(int height);
    public void setPitch(float pitch);
    public void setBatteryPercentage(int percentage);
}
