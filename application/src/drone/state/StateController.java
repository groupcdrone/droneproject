/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.state;

import de.yadrone.base.ARDrone.ISpeedListener;
import de.yadrone.base.navdata.Altitude;
import de.yadrone.base.navdata.AltitudeListener;
import de.yadrone.base.navdata.AttitudeListener;
import de.yadrone.base.navdata.BatteryListener;
import de.yadrone.base.navdata.ControlState;
import de.yadrone.base.navdata.DroneState;
import de.yadrone.base.navdata.StateListener;
import drone.Controller;
import drone.model.Drone;

/**
 *
 * @author Mike
 */
public class StateController extends Controller implements ISpeedListener {

    private final StateView stateView;
    private final Drone drone;

    public StateController(StateView sv) {
        super(sv);
        stateView = sv;

        drone = Drone.getInstance();
        setInitialState();
        setStateListeners();
    }

    private void setInitialState() {
        stateView.setFlyingState(false);
        stateView.setSpeed(0);
        stateView.setAltitude(0);
        stateView.setPitch(0);
        stateView.setBatteryPercentage(0);
    }

    private void setStateListeners() {

        drone.getNavDataManager().addStateListener(new StateListener() {

            private String state = "";
            private boolean flying = false;

            @Override
            public void stateChanged(DroneState data) {
                
                if (data.isFlying() != flying) {
                    flying = !flying;
                    stateView.setFlyingState(flying);
                }

                if (!state.equals(data.toString())) {
                    state = data.toString();
                }
            }

            @Override
            public void controlStateChanged(ControlState state) {
            }

        });
        
        drone.getNavDataManager().addAltitudeListener(new AltitudeListener() {

            private int height;

            @Override
            public void receivedAltitude(int i) {
                if (height != i) {
                    height = i;

                    stateView.setAltitude(height);
                }
            }

            @Override
            public void receivedExtendedAltitude(Altitude altd) {
            }
        });

        drone.getNavDataManager().addAttitudeListener(new AttitudeListener() {

            private float currentPitch;

            @Override
            public void attitudeUpdated(float pitch, float roll, float yaw) {
                if (currentPitch != pitch) {
                    currentPitch = pitch;
                    stateView.setPitch(currentPitch);
                }
            }

            @Override
            public void attitudeUpdated(float pitch, float roll) {
            }

            @Override
            public void windCompensation(float pitch, float roll) {
            }
        });

        drone.getNavDataManager().addBatteryListener(new BatteryListener() {

            private int batteryPercentage;

            @Override
            public void batteryLevelChanged(int percentage) {
                if (batteryPercentage != percentage) {
                    batteryPercentage = percentage;

                    stateView.setBatteryPercentage(batteryPercentage);

                    System.out.println("Battery: " + percentage + " %");
                }
            }

            @Override
            public void voltageChanged(int vbat_raw) {
            }
        });
    }

    @Override
    public void speedUpdated(int i) {
        stateView.setSpeed(i);
    }

}
