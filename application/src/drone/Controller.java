/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone;

import javax.swing.JPanel;

/**
 *
 * @author Mike
 */
public abstract class Controller {

    private View view;
    private static JPanel topPanel;

    public Controller(View v) {
        view = v;
    }

    public Controller() {

    }

    public void showNextPane(String pane) {
        view.showNextPane(pane);
    }

    /* 
     * Must be static in order to always get the same value of topPanel
     */
    public static JPanel getTopPanel() {
        return topPanel;
    }

    public void setTopPanel(JPanel tp) {
        topPanel = tp;
    }
}
