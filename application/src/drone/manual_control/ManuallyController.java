/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.manual_control;

import de.yadrone.base.command.CommandManager;
import drone.Controller;
import drone.model.Drone;

/**
 *
 * @author Tim Oosterbroek
 */
public final class ManuallyController extends Controller {

    private static ManuallyController ins;

    private final ManuallyView manuallyView;
    private final Drone drone;
    private final CommandManager cmd;

    private final int speed; // percentage of max speed
    public boolean started;
    private boolean landed;


    /**
     * Creates a new ManuallyController if it is null
     * @param mv
     * @return an instance of ManuallyController
     */
    public static ManuallyController getInstance(ManuallyView mv) {
        if (ins == null) {
            ins = new ManuallyController(mv);
        }
        return ins;
    }

    private ManuallyController(ManuallyView mv) {
        super(mv);
        manuallyView = mv;

        drone = Drone.getInstance();
        cmd = drone.getCommandManager();

        speed = 5;
        started = false;
        landed = false;
    }

    /**
     * drone goes up
     */
    public void goUp() {
        cmd.up(speed);
    }

    /**
     * drone goes down
     */
    public void goDown() {
        cmd.down(speed);
    }

    /**
     * drone goes to the left
     */
    public void goLeft() {
        cmd.goLeft(speed);
    }

    /**
     * drone goes to the right
     */
    public void goRight() {
        cmd.goRight(speed);

    }

    /**
     * drone goes forward
     */
    public void goForward() {
        cmd.forward(speed);
    }

    /**
     *  drone goes backward
     */
    public void goBackward() {
        cmd.backward(speed);
    }

    /**
     * drone spins to the left
     */
    public void spinLeft() {
        cmd.spinLeft(speed);
    }

    /**
     * drones spins to the right
     */
    public void spinRight() {
        cmd.spinRight(speed);
    }

    /**
     * drone calibrates
     */
    public void calibrate() {

    }

    /**
     * if drone hasn't started, drone takes off
     */
    public void takeOff() {
        if (!started) {
            started = true;
            landed = false;

            cmd.takeOff().hover();
        }
    }

    /**
     * If drone is landend, drone lands
     */
    public void land() {
        if (!landed) {
            landed = true;
            started = false;

            cmd.landing();
        }
    }

    /**
     * If drone is started, drone lands, else drone takes off
     */
    public void checkStarted(){
        if(started) {
            land();
        }
        else {
            takeOff();
        }
    }

    /**
     * Hover when there are no keys pressed
     * Hover when a key is released
     */
    public void hover() {
        cmd.hover();
    }

    /**
     * drone crashes
     */
    public void emergency() {
        cmd.emergency();
    }

}
