/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.follow_route;

import api.APIHandler;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import de.yadrone.base.command.CommandManager;
import de.yadrone.base.command.LEDAnimation;
import de.yadrone.base.command.VideoChannel;
import drone.Controller;
import drone.QRCodeScanner;
import drone.ShelfCountWithoutEmptySpaces;
import drone.TagObserver;
import drone.VideoObserver;
import drone.model.Drone;
import drone.model.FlightInstruction;
import drone.model.Route;
import drone.video.VideoController;
import drone.video.VideoPane;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Mike
 */
public class FollowRouteController extends Controller implements TagObserver, VideoObserver {

    private final FollowRouteView followRouteView;
    private final Route route;

    private final Drone drone;
    private final CommandManager cmd;
    private final int speed = 6;
    private final ArrayList<Result> scannedResults;

    private ScheduledFuture scheduledCommand;
    private final int scheduledCommandTime = 10; // ms
    private ArrayList<FlightInstruction> instructions;
    private int currentFlightInstruction;
    private String QRCodeToBeScanned;

    private String direction = "moveForward";

    public FollowRouteController(FollowRouteView frv, Route routeToFollow) {
        super(frv);
        followRouteView = frv;
        route = routeToFollow;
        drone = Drone.getInstance();
        cmd = drone.getCommandManager();

        scannedResults = new ArrayList<>();
        VideoController.getInstance().registerObserver(this);

        getFlightInstructions();
        startFlying();
    }

    /**
     * When the emergency button gets pressed
     */
    public void emergency() {
        cmd.emergency();
    }

    /**
     * Get the flight instructions
     */
    private void getFlightInstructions() {
        APIHandler api = APIHandler.getInstance();
        instructions = api.getFlightInstructions(route.getID());

        sortFlightInstructionsByInstructionNumber();
    }

    /**
     * Insertion sort on the instruction number
     * Since it will always be small amounts, insertion sort is the fastest 
     */
    private void sortFlightInstructionsByInstructionNumber() {
        for (int p = 1; p < instructions.size(); p++) {
            FlightInstruction tmp = instructions.get(p);
            int j = p;

            for (; j > 0 && tmp.getInstructionNumber() < instructions.get(j - 1).getInstructionNumber(); j--) {
                instructions.set(j, instructions.get(j - 1));
            }
            instructions.set(j, tmp);
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < instructions.size(); i++) {
            sb.append(instructions.get(i).getFlightInstructID() + " ");
        }
        System.out.println(sb.toString());
    }

    /**
     * After selecting the route, start flying
     */
    private void startFlying() {
        cmd.takeOff().waitFor(2000);
        cmd.hover().doFor(1000);

        currentFlightInstruction = -1;
        QRCodeToBeScanned = instructions.get(currentFlightInstruction + 1).getInstructionCode();

        // Now we wait until the onTag method gets called
        QRCodeScanner.getInstance().registerObserver(this);
        cmd.setVideoChannel(VideoChannel.VERT);
    }

    @Override
    public void onTag(Result result, float f) {
        if (result != null) {
            boolean exists = false;

            for (Result r : scannedResults) {
                if (r.getText().equals(result.getText())) {
                    exists = true;
                }
            }
            if (!exists) {
                scannedResults.add(result);

                // If the QR code we scan is the next QR code in the route, then perform the instruction
                if (result.getText().equals(QRCodeToBeScanned)) {
                    doFlightInstruction(result, f);
                }
            }
        }
    }

    /**
     * Do a fly instruction
     * @param result Value from QRCodeScanner
     * @param f Orientation on the QR code
     */
    private void doFlightInstruction(Result result, float f) {
        // First we are going to center ourselves on the tag
        while (!isTagCentered(result, f)) {
            centerTag(result, f);
        }

        // Update the currentFlightInstruction
        currentFlightInstruction++;

        // Check if there after this there will be another flight instruction
        if (currentFlightInstruction + 1 < instructions.size()) {
            QRCodeToBeScanned = instructions.get(currentFlightInstruction + 1).getInstructionCode();
        }

        // Get the flight instruction, cancel the previous one, check for waypoints and schedule the correct commands
        FlightInstruction fi = instructions.get(currentFlightInstruction);
        // cancelPreviousFlightInstruction();

        if (!checkForWaypoint(fi)) {
            direction = fi.getDescription();
            scheduleCommands(fi);
        }
    }

    /**
     * Checks if tag is centered
     * @param tag scanned QR code
     * @param tagOrientation Orientation on the QR code in degrees
     * @return Returns if the tag is centerd
     */
    private boolean isTagCentered(Result tag, float tagOrientation) {
        if (tag == null) {
            return false;
        }

        // A tag is centered if it is
        // 1. if "Point 1" (on the tag the upper left point) is near the center of the camera
        // 2. orientation is between 350 and 10 degrees
        int imgCenterX = VideoPane.DRAWING_WIDTH / 2;
        int imgCenterY = VideoPane.DRAWING_HEIGTH / 2;

        ResultPoint[] points = tag.getResultPoints();
        boolean isCentered = ((points[1].getX() > (imgCenterX - VideoPane.TOLERANCE))
                && (points[1].getX() < (imgCenterX + VideoPane.TOLERANCE))
                && (points[1].getY() > (imgCenterY - VideoPane.TOLERANCE))
                && (points[1].getY() < (imgCenterY + VideoPane.TOLERANCE)));

        boolean isOriented = ((tagOrientation < 10) || (tagOrientation > 350));

        if (isCentered && isOriented) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Center the tag
     * @param tag the QR code
     * @param tagOrientation Orientation on the QR code
     */
    private void centerTag(Result tag, float tagOrientation) {
        String tagText;
        ResultPoint[] points;

        synchronized (tag) {
            points = tag.getResultPoints();
            tagText = tag.getText();
        }

        int imgCenterX = VideoPane.DRAWING_WIDTH / 2;
        int imgCenterY = VideoPane.DRAWING_HEIGTH / 2;

        float x = points[1].getX();
        float y = points[1].getY();

        if ((tagOrientation > 10) && (tagOrientation < 180)) {
            cmd.setEnableCombinedYaw(false);
            cmd.move(0, 0, 0, -speed).doFor(500);
        } else if ((tagOrientation < 350) && (tagOrientation > 180)) {
            cmd.setEnableCombinedYaw(false);
            cmd.move(0, 0, 0, -speed).doFor(500);
        } else if (x < (imgCenterX - VideoPane.TOLERANCE)) {
            cmd.goLeft(speed).doFor(500);
        } else if (x > (imgCenterX + VideoPane.TOLERANCE)) {
            cmd.goRight(speed).doFor(500);
        } else if (y < (imgCenterY - VideoPane.TOLERANCE)) {
            cmd.forward(speed).doFor(500);
        } else if (y > (imgCenterY + VideoPane.TOLERANCE)) {
            cmd.backward(speed).doFor(500);
        } else {
            cmd.setLedsAnimation(LEDAnimation.BLINK_GREEN, 10, 5);
        }
    }

    private void cancelPreviousFlightInstruction() {
        if (!scheduledCommand.isCancelled()) {
            scheduledCommand.cancel(true);
            cmd.hover().doFor(2000);
        }
    }

    /**
     * Checks if flightinstuction is waypoint
     * @param fi FLyinstruction
     * @return Returns if flyinstruction is waypoint
     */
    private boolean checkForWaypoint(FlightInstruction fi) {
        // Check if the second word equals "waypoint"
        if (fi.getDescription().split(" ")[1].equals("waypoint")) {
            // We've hit a waypoint, so we need to count the shelf
            QRCodeScanner.getInstance().removeObserver(this);
            ShelfCountWithoutEmptySpaces sc = new ShelfCountWithoutEmptySpaces();
            sc.setFollowRouteController(this);
            drone.setShelfCountBehavior(sc);
            sc.countShelf();

            return true;
        } else {
            return false;
        }
    }

    /**
     * Does the flyinstruction
     * @param flightInstruction the specified flyInstruction
     */
    private void scheduleCommands(FlightInstruction flightInstruction) {
        final FlightInstruction fi = flightInstruction;

        // Every X miliseconds, do the command based on the flightinstruction
        scheduledCommand = Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                // Switch on the first word of the description
                switch (fi.getDescription().split(" ")[0]) {
                    case "moveForward":
                        cmd.forward(speed);
                        break;
                    case "moveRight":
                        cmd.goRight(speed);
                        break;
                    case "moveBackward":
                        cmd.backward(speed);
                        break;
                    case "moveLeft":
                        cmd.goLeft(speed);
                        break;
                    case "landing":
                        // This means we are done with the route!
                        cmd.landing();
                        scheduledCommand.cancel(false);
                        break;
                    default:
                        break;
                }
            }
        }, 0, scheduledCommandTime, TimeUnit.MILLISECONDS);
    }

    /**
     * Continue route after counting
     */
    public void continueRoute() {
        QRCodeScanner.getInstance().registerObserver(this);

        FlightInstruction fi = instructions.get(currentFlightInstruction);
        scheduleCommands(fi);
    }

    /**
     * Function to follow the line
     * @param x x position of line
     * @param y y position of line
     */
    @Override
    public void gridUpdate(int x, int y) {
        System.out.println(x + " " + y);
        if (direction.equals("moveForward") || direction.equals("moveBackward")) {
            switch (x) {
                case 0:
                    cmd.goLeft(1);
                    break;
                case 1:
                    if (direction.equals("moveForward")) {
                        cmd.forward(1);
                    } else {
                        cmd.backward(1);
                    }
                    break;
                case 2:
                    cmd.goRight(1);
                    break;
                default:
                    System.out.println("No correct fly instruction!");
            }
        } else if (direction.equals("moveLeft") || direction.equals("moveRight")) {
            switch (y) {
                case 0:
                    cmd.forward(1);
                    break;
                case 1:
                    if (direction.equals("moveRight")) {
                        cmd.goRight(1);
                    } else {
                        cmd.goLeft(1);
                    }
                    break;
                case 2:
                    cmd.backward(1);
                    break;
                default:
                    System.out.println("No correct instruction!");
            }
        }
    }
}
