package drone;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import de.yadrone.base.video.ImageListener;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class QRCodeScanner implements ImageListener, Subject {

    private static QRCodeScanner ins;

    private final ArrayList<TagObserver> observers; 

    private Result scanResult;
    private double theta;
    private long imageCount = 0;

    public static QRCodeScanner getInstance() {
        if (ins == null) {
            ins = new QRCodeScanner();
        }
        return ins;
    }

    private QRCodeScanner() {
        observers = new ArrayList<TagObserver>();
    }

    @Override
    public void imageUpdated(BufferedImage image) {
        if ((++imageCount % 2) == 0) {
            return;
        }

        // try to detect QR code
        LuminanceSource source = new BufferedImageLuminanceSource(image);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        QRCodeReader reader = new QRCodeReader();

        theta = Double.NaN;
        try {
            scanResult = reader.decode(bitmap);

            ResultPoint[] points = scanResult.getResultPoints();
            ResultPoint a = points[1]; // top-left
            ResultPoint b = points[2]; // top-right

            // Find the degree of the rotation (needed e.g. for auto control)
            double z = Math.abs(a.getX() - b.getX());
            double x = Math.abs(a.getY() - b.getY());
            theta = Math.atan(x / z); // degree in rad (+- PI/2)

            theta = theta * (180 / Math.PI); // convert to degree

            if ((b.getX() < a.getX()) && (b.getY() > a.getY())) { // code turned more than 90� clockwise
                theta = 180 - theta;
            } else if ((b.getX() < a.getX()) && (b.getY() < a.getY())) { // code turned more than 180� clockwise
                theta = 180 + theta;
            } else if ((b.getX() > a.getX()) && (b.getY() < a.getY())) { // code turned more than 270 clockwise
                theta = 360 - theta;
            }
        } catch (ReaderException e) {
            // no code found.
            scanResult = null;
        }

        // inform all listener
        notifyObservers();
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add((TagObserver) o);
    }

    @Override
    public void removeObserver(Observer o) {
        int i = observers.indexOf((TagObserver) o);
        if (i >= 0) {
            observers.remove(i);
        }
    }

    @Override
    public void notifyObservers() {
        for (TagObserver observer : observers) {
            observer.onTag(scanResult, (float) theta);
        }
    }
}
