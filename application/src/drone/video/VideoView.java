/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.video;

import com.google.zxing.Result;
import drone.View;

/**
 *
 * @author Mike
 */
public interface VideoView extends View {
    public VideoPane getVideoPane();
    public void onTag(Result result, float f);
}
