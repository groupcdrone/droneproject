/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.video;

import com.google.zxing.Result;
import de.yadrone.base.command.VideoChannel;
import drone.*;
import drone.model.Drone;

import java.util.ArrayList;

/**
 *
 * @author Mike
 */
public final class VideoController extends Controller implements TagObserver, Subject {

    private static VideoController ins;
    private final VideoView videoView;
    private final Drone drone;

    private final ArrayList<VideoObserver> observers;
    private final ArrayList<QRGridObserver> QRObservers;
    private int gridX;
    private int gridY;

    private boolean isVideoObserver = false;
    private boolean isQRgridObserver = false;

    public VideoController(VideoView vv) {
        super(vv);
        videoView = vv;
        drone = Drone.getInstance();

        drone.getCommandManager().setVideoChannel(VideoChannel.VERT);
        drone.getVideoManager().addImageListener(vv.getVideoPane());

        QRCodeScanner.getInstance().registerObserver(this);
        drone.getVideoManager().addImageListener(QRCodeScanner.getInstance());

        observers = new ArrayList<>();
        QRObservers = new ArrayList<>();
        ins = this;
    }

    public static VideoController getInstance(){
        return ins;
    }

    public void changeVideoChannel() {
        drone.getCommandManager().setVideoChannel(VideoChannel.NEXT);
    }

    /**
     * Methods for updating corresponding observers with gridvalues based on where a QR code or tracker was detected
     * XY axis is as following; 0,0 upper left rectangle, 1,0 mid-upper rectangle 1,1 is center rectangle, etc.
     */
    public void updateGridValues(int gridX, int gridY) {
        this.gridX = gridX;
        this.gridY = gridY;
        isVideoObserver = true;
        notifyObservers();
    }

    public void updateQRGridValues(int gridX, int gridY) {
        this.gridX = gridX;
        this.gridY = gridY;
        isQRgridObserver = true;
        notifyObservers();
    }

    @Override
    public void onTag(Result result, float f) {
        videoView.onTag(result, f);
    }

    @Override
    public void registerObserver(Observer o) {
        if(o instanceof VideoObserver){
            observers.add((VideoObserver) o);
        }else{
            QRObservers.add((QRGridObserver) o);
        }

    }

    @Override
    public void removeObserver(Observer o) {
        if(o instanceof VideoObserver){
            observers.remove((VideoObserver) o);
        }else{
            QRObservers.remove((QRGridObserver) o);
        }

    }

    @Override
    public void notifyObservers() {
        if(isVideoObserver) {
            isVideoObserver = false;
            for (VideoObserver o : observers) {
                o.gridUpdate(gridX, gridY);
            }
        }

        if(isQRgridObserver) {
            isQRgridObserver = false;
            for (QRGridObserver o : QRObservers) {
                o.QRGridUpdate(gridX, gridY);
            }
        }
    }
}
