/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.route_list;

import api.APIHandler;
import drone.Controller;
import drone.model.Route;
import java.util.ArrayList;

/**
 *
 * @author Mike
 */
public class RouteListController extends Controller {

    private final RouteListView routeListView;

    private final APIHandler apiHandler;
    private final ArrayList<Route> routes;
    
    private static Route selectedRoute;

    public RouteListController(RouteListView rlv) {
        super(rlv);
        routeListView = rlv;
        apiHandler = APIHandler.getInstance();
        routes = apiHandler.getRoutes();

        if (routes != null) {
            setRoutesInPane();
        }
    }

    private void setRoutesInPane() {
        routeListView.setRoutesInPane(routes);
    }
    
    public void setSelectedRoute(Route selectedRoute) {
        RouteListController.selectedRoute = selectedRoute;
    }
    
    public static Route getSelectedRoute() {
        return selectedRoute;
    }
}
