/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.route_list;

import drone.model.Route;
import drone.View;
import java.util.ArrayList;

/**
 *
 * @author Mike
 */
public interface RouteListView extends View {
    public void setRoutesInPane(ArrayList<Route> routes);
    public void setNewSelectedRoute(RouteItem item);
    public void setNewDescription();
}
