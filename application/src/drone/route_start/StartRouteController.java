/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.route_start;

import drone.Controller;

/**
 *
 * @author Mike
 */
public class StartRouteController extends Controller {
    
    private static StartRouteController ins;
    private final StartRouteView selectRouteView;
    
    public static StartRouteController getInstance(StartRouteView srv) {
        if (ins == null) {
            ins = new StartRouteController(srv);
        }
        return ins;
    }
    
    private StartRouteController(StartRouteView srv) {
        super(srv);
        selectRouteView = srv;
    }  
}
