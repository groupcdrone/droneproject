/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.route_start;

import drone.route_list.RouteListPane;
import java.awt.CardLayout;
import drone.Controller;
import drone.follow_route.FollowRoutePane;
import drone.route_list.RouteListController;
import javax.swing.JPanel;

/**
 *
 * @author Mike
 */
public final class StartRoutePane extends javax.swing.JPanel implements StartRouteView {

    private StartRouteController controller;

    /**
     * Creates new form StartRouteView
     */
    public StartRoutePane() {
        initComponents();
        setController(StartRouteController.getInstance(this));

        routeListPanel.add(new RouteListPane());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        titleLabel = new javax.swing.JLabel();
        fillerPanel = new javax.swing.JPanel();
        backBtn = new javax.swing.JButton();
        contentPanel = new javax.swing.JPanel();
        startBtn = new javax.swing.JButton();
        routeListPanel = new javax.swing.JPanel();

        setBackground(new java.awt.Color(153, 255, 153));
        setMaximumSize(new java.awt.Dimension(1600, 900));
        setMinimumSize(new java.awt.Dimension(1600, 900));
        setName(""); // NOI18N
        setOpaque(false);
        setPreferredSize(new java.awt.Dimension(1600, 900));
        setLayout(new java.awt.GridLayout(3, 0));

        titleLabel.setFont(new java.awt.Font("Calibri", 1, 48)); // NOI18N
        titleLabel.setForeground(new java.awt.Color(255, 255, 255));
        titleLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleLabel.setText("SELECT ROUTE");
        titleLabel.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        titleLabel.setRequestFocusEnabled(false);
        add(titleLabel);

        fillerPanel.setBackground(new java.awt.Color(0, 150, 135));
        fillerPanel.setMaximumSize(new java.awt.Dimension(1600, 300));
        fillerPanel.setMinimumSize(new java.awt.Dimension(1600, 300));
        fillerPanel.setName(""); // NOI18N
        fillerPanel.setPreferredSize(new java.awt.Dimension(1600, 300));
        fillerPanel.setLayout(null);

        backBtn.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        backBtn.setForeground(new java.awt.Color(255, 255, 255));
        backBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/ic_arrow_back_white_18dp_1x.png"))); // NOI18N
        backBtn.setText("BACK");
        backBtn.setBorder(null);
        backBtn.setContentAreaFilled(false);
        backBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        backBtn.setFocusPainted(false);
        backBtn.setMargin(new java.awt.Insets(8, 20, 8, 20));
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });
        fillerPanel.add(backBtn);
        backBtn.setBounds(50, 30, 70, 50);

        contentPanel.setOpaque(false);
        contentPanel.setPreferredSize(new java.awt.Dimension(1600, 300));
        contentPanel.setLayout(new java.awt.GridBagLayout());

        startBtn.setBackground(new java.awt.Color(0, 117, 106));
        startBtn.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        startBtn.setForeground(new java.awt.Color(255, 255, 255));
        startBtn.setText("START");
        startBtn.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        startBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        startBtn.setFocusPainted(false);
        startBtn.setMaximumSize(new java.awt.Dimension(250, 39));
        startBtn.setMinimumSize(new java.awt.Dimension(250, 39));
        startBtn.setPreferredSize(new java.awt.Dimension(250, 39));
        startBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startBtnActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        contentPanel.add(startBtn, gridBagConstraints);

        routeListPanel.setMaximumSize(new java.awt.Dimension(600, 260));
        routeListPanel.setMinimumSize(new java.awt.Dimension(600, 260));
        routeListPanel.setOpaque(false);
        routeListPanel.setPreferredSize(new java.awt.Dimension(600, 260));
        routeListPanel.setLayout(new javax.swing.BoxLayout(routeListPanel, javax.swing.BoxLayout.LINE_AXIS));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        contentPanel.add(routeListPanel, gridBagConstraints);

        fillerPanel.add(contentPanel);
        contentPanel.setBounds(0, 0, 1600, 300);

        add(fillerPanel);

        getAccessibleContext().setAccessibleParent(this);
    }// </editor-fold>//GEN-END:initComponents

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
        controller.showNextPane("STARTUP");
    }//GEN-LAST:event_backBtnActionPerformed

    private void startBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startBtnActionPerformed
        initializeAllPanes();
        controller.showNextPane("FOLLOWROUTE");
    }//GEN-LAST:event_startBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backBtn;
    private javax.swing.JPanel contentPanel;
    private javax.swing.JPanel fillerPanel;
    private javax.swing.JPanel routeListPanel;
    private javax.swing.JButton startBtn;
    private javax.swing.JLabel titleLabel;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setController(Controller controller) {
        this.controller = (StartRouteController) controller;
    }

    @Override
    public void showNextPane(String pane) {
        CardLayout cl = (CardLayout) StartRouteController.getTopPanel().getLayout();
        cl.show(StartRouteController.getTopPanel(), pane);
    }

    @Override
    public void initializeAllPanes() {
        JPanel followRoute = new FollowRoutePane(RouteListController.getSelectedRoute());
        StartRouteController.getTopPanel().add(followRoute, "FOLLOWROUTE");
    }

}
