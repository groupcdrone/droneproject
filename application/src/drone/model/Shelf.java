/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.model;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author erikj
 */
public class Shelf {
    
    @SerializedName("ShelfID")
    private int ID;
    
    @SerializedName("ShelfCode")
    private final String shelfCode;
    
    @SerializedName("ProductID")
    private final int productID;
   
    @SerializedName("ShelfHeight")
    private final double shelfHeight;
    
    @SerializedName("ShelfWidth")
    private final double shelfWidth;
    
    @SerializedName("ShelfSeparatorHeight")
    private final double shelfSeparatorHeight;
    
    @SerializedName("NumberOfRows")
    private final int numberOfRows;
    
    public Shelf(int ID, String shelfCode, int productID, double shelfHeight, double shelfWidth, double shelfSeparatorHeight, int numberOfRows){
        this.ID                   = ID;
        this.shelfCode            = shelfCode;
        this.productID            = productID;
        this.shelfHeight          = shelfHeight;
        this.shelfWidth           = shelfWidth;
        this.shelfSeparatorHeight = shelfSeparatorHeight;
        this.numberOfRows         = numberOfRows;
    }

    /**
     * @return the ID
     */
    public int getID() {
        return ID;
    }

    /**
     * @return the productID
     */
    public int getProductID() {
        return productID;
    }

    /**
     * @return the shelfHeight
     */
    public double getShelfHeight() {
        return shelfHeight;
    }

    /**
     * @return the shelfWidth
     */
    public double getShelfWidth() {
        return shelfWidth;
    }

    /**
     * @return the shelfCode
     */
    public String getShelfCode() {
        return shelfCode;
    }

    /**
     * @return the shelfSeparatorHeight
     */
    public double getShelfSeparatorHeight() {
        return shelfSeparatorHeight;
    }

    /**
     * @return the numberOfRows
     */
    public int getNumberOfRows() {
        return numberOfRows;
    }
}
