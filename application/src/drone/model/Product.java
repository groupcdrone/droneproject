/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.model;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author erikj
 */
public class Product {
    
    @SerializedName("ProductID")
    private int ID;

    @SerializedName("ProductCode")
    private String productCode;
    
    @SerializedName("ProductName")
    private String name;
        
    @SerializedName("ProductHeight")
    private double height;
    
    @SerializedName("ProductWidth")
    private double width;
    
    @SerializedName("Amount")
    private int amount;
    
    public Product(int id, String productCode, String name,  double height, double width, int amount){
        this.ID          = id;
        this.productCode = productCode;
        this.name        = name;
        this.height      = height;
        this.width       = width;
        this.amount      = amount;
    }

    /**
     * @return the ID
     */
    public int getID() {
        return ID;
    }

    /**
     * @return the height
     */
    public double getHeight() {
        return height;
    }

    /**
     * @return the width
     */
    public double getWidth() {
        return width;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the amount
     */
    public int getAmount() {
        return amount;
    }

 
    
}
