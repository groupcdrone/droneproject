/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.model;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author erikj
 */
public class FlightInstruction {
    
    @SerializedName("FlightInstructID")
    private int FlightInstructID;
    
    @SerializedName("RouteID")
    private int RouteID;
    
    @SerializedName("InstructionNumber")
    private int InstructionNumber;
    
    @SerializedName("InstructionCode")
    private String InstructionCode;
    
    @SerializedName("Description")
    private String Description;
    

    
    public FlightInstruction(int FlightInstructID, int RouteID, int InstructionNumber, 
                             String InstructionCode, String Description){
        
        this.FlightInstructID  = FlightInstructID;
        this.RouteID           = RouteID;
        this.InstructionNumber = InstructionNumber;
        this.InstructionCode   = InstructionCode;
        this.Description       = Description;  
        
    }
        

    /**
     * @return the FlightInstructID
     */
    public int getFlightInstructID() {
        return FlightInstructID;
    }

    /**
     * @param FlightInstructID the FlightInstructID to set
     */
    public void setFlightInstructID(int FlightInstructID) {
        this.FlightInstructID = FlightInstructID;
    }

    /**
     * @return the RouteID
     */
    public int getRouteID() {
        return RouteID;
    }

    /**
     * @return the InstructionNumber
     */
    public int getInstructionNumber() {
        return InstructionNumber;
    }

    /**
     * @return the InstructionCode
     */
    public String getInstructionCode() {
        return InstructionCode;
    }

    /**
     * @return the Description
     */
    public String getDescription() {
        return Description;
    }


}
