/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.model;

import de.yadrone.base.ARDrone;
import de.yadrone.base.IARDrone;
import de.yadrone.base.command.CommandManager;
import de.yadrone.base.command.FlyingMode;
import de.yadrone.base.command.KeepAliveCommand;
import de.yadrone.base.configuration.ConfigurationManager;
import de.yadrone.base.exception.ARDroneException;
import de.yadrone.base.exception.IExceptionListener;
import de.yadrone.base.navdata.NavDataManager;
import de.yadrone.base.video.VideoManager;
import drone.ShelfCountBehavior;

public class Drone {

    private static Drone ins;
    private IARDrone drone;
    
    private final CommandManager commandManager; 
    private final NavDataManager navDataManager; 
    private final VideoManager videoManager;
    private final ConfigurationManager configurationManager;
    
    private final FlyingMode flyingMode;
    private final boolean combinedYawEnabled;
    
    private ShelfCountBehavior shelfCountBehavior;

    public static Drone getInstance() {
        if (ins == null) {
            ins = new Drone();
        }
        return ins;
    }

    private Drone() {
        drone = null;
        initDrone();
        
        commandManager = drone.getCommandManager();
        navDataManager = drone.getNavDataManager();
        videoManager = drone.getVideoManager();
        configurationManager = drone.getConfigurationManager();
        
        flyingMode = FlyingMode.FREE_FLIGHT;
        commandManager.setFlyingMode(flyingMode);
        
        combinedYawEnabled = false;
        commandManager.setEnableCombinedYaw(combinedYawEnabled);
    }

    private void initDrone() {
        try {
            drone = new ARDrone();
            drone.addExceptionListener(new IExceptionListener() {
                @Override
                public void exeptionOccurred(ARDroneException exc) {
                    //exc.printStackTrace();
                }
            });
            drone.start();
        } catch (Exception exc) {
            drone.stop();
            // exc.printStackTrace();
        }
    }

    public IARDrone getIARDrone() {
        return drone;
    }
    
    public CommandManager getCommandManager() {
        return commandManager;
    }

    public NavDataManager getNavDataManager() {
        return navDataManager;
    }

    public VideoManager getVideoManager() {
        return videoManager;
    }
    
    public ConfigurationManager getConfigurationManager() {
        return configurationManager;
    }
    
    public void performShelfCount() {
        shelfCountBehavior.countShelf();
    }
    
    public void setShelfCountBehavior(ShelfCountBehavior sb) {
        shelfCountBehavior = sb;
    }

}
