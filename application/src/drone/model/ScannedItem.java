package drone.model;

import com.google.zxing.Result;

/**
 * Created by Robert on 09-Dec-16.
 */
public class ScannedItem {
    Result result;
    int count;

    public ScannedItem(Result result, int count){
        this.count = count;
        this.result = result;
    }

    public Result getResult() {
        return result;
    }

    public int getCount() {
        return count;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
