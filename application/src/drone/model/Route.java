/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.model;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 *
 * @author erikj
 */
public class Route {

    @SerializedName("RouteID")
    private int ID;
    
    @SerializedName("Name")
    private String name;
    
    @SerializedName("Description")
    private String description;

    private ArrayList<FlightInstruction> instructionSet; 

    public Route(int id, String name, String description) {
        this.ID          = id;
        this.name        = name;
        this.description = description;
    }
    
    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
    

}
