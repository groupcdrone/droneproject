/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone;

import com.google.zxing.Result;
import de.yadrone.base.command.CommandManager;
import api.APIHandler;
import de.yadrone.base.command.VideoChannel;
import drone.follow_route.FollowRouteController;
import drone.model.Drone;
import drone.model.Product;
import drone.model.Shelf;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Mike
 */
public class ShelfCountWithEmptySpaces implements ShelfCountBehavior {

    private FollowRouteController followRouteController;
    private final ArrayList<Result> scannedResults;

    private int totalAmountOfEmptyPlaces;

    private boolean shelfCodeWasScanned;
    private boolean productWasScanned;
    private final int secondsToScanTheProduct = 1;
    private ScheduledFuture scheduledFuture;

    private final double timeToFlyOneMiliMeter = 10 / 3; // ms

    private Shelf shelf;
    private Product product;

    private double maxProductsVerticalPerRow;
    private double maxProductsHorizontalPerShelf;

    private int currentVerticalPosition;
    private int currentRow;
    private int currentHorizontalPosition;

    private final CommandManager cmd;
    private final int speed = 10; // percentage of max speed

    public ShelfCountWithEmptySpaces() {
        scannedResults = new ArrayList<>();
        cmd = Drone.getInstance().getCommandManager();
        cmd.setVideoChannel(VideoChannel.VERT);

        shelfCodeWasScanned = false;
        productWasScanned = false;

        totalAmountOfEmptyPlaces = 0;
    }

    public void setFollowRouteController(FollowRouteController fc) {
        followRouteController = fc;
    }

    @Override
    public void countShelf() {
        QRCodeScanner.getInstance().registerObserver(this);
    }

    @Override
    public void onTag(Result result, float f) {
        if (result != null) {
            boolean exists = false;

            for (Result r : scannedResults) {
                if (r.getText().equals(result.getText())) {
                    exists = true;
                }
            }
            if (!exists) {
                scannedResults.add(result);

                // First time the function gets here he should've scanned a Shelf QR-code
                if (!shelfCodeWasScanned) {
                    shelfCodeWasScanned = true;
                    QRCodeScanner.getInstance().removeObserver(this);
                    cmd.setVideoChannel(VideoChannel.HORI);

                    getProductInfoFromShelf(result);
                } else {
                    // Else it should be a product
                    productWasScanned = true;

                    // If he scans a product, then we don't have to wait for the timer to finish, just continue directly
                    stopTimer();
                }
            }
        }
    }

    private void getProductInfoFromShelf(Result result) {
        APIHandler api = APIHandler.getInstance();
        shelf = api.getShelfInfo(result.getText());
        product = api.getProductInforById(shelf.getProductID());

        double heightOfRow = (shelf.getShelfHeight() - ((shelf.getNumberOfRows() - 1) * shelf.getShelfSeparatorHeight())) / shelf.getNumberOfRows();
        maxProductsVerticalPerRow = Math.floor(heightOfRow / product.getHeight());
        maxProductsHorizontalPerShelf = Math.floor(shelf.getShelfWidth() / product.getWidth());

        startCounting();
    }

    private void startCounting() {
        /*
            Default case: QR codes are always in the middle of a product
            1. Get product info from shelf
            2. Get to height of first product, and scan
            3. Get to height of product above, and scan
            4. Repeat step 3 till it is certain that there won't be any products above (check with shell height)
            5. Fly to the right (product width), and completely down again
            6. Repeat step 2-5 till it is certain that there won't be any products on the right (check with shell width)
            7. Show the result of counted items against database
         */

        flyToFirstProduct();
    }

    private void flyToFirstProduct() {
        /*
            Default altitude of the drone is 1000mm
            1. Fly a bit downwards (product height / 2)
            2. Fly to the right (product width / 2)
            3. Scan the product
         */

        double milimetersToGoDown = 1000 - ((product.getHeight() * 10) / 2);
        double milimetersToGoRight = (product.getWidth() * 10) / 2;

        cmd.down(speed * (3 / 2)).doFor((long) (milimetersToGoDown * timeToFlyOneMiliMeter));
        cmd.hover().doFor(2000);
        cmd.goRight(speed / 5).doFor((long) (milimetersToGoRight * timeToFlyOneMiliMeter));
        cmd.hover().doFor(2000);

        currentVerticalPosition = 1;
        currentRow = 1;
        currentHorizontalPosition = 1;

        scanProduct();
    }

    private void flyToNextProduct(int emptyPlaces) {
        /*
            1. Fly upwards based on product height * emptyPlaces
            2. Check if flying upwards doesn't exceed the shelf height
                - If not: scan the product
                - Else: fly to the right based on product width
         */

        // Set the new vertical position
        if (emptyPlaces == 0) {
            currentVerticalPosition = currentVerticalPosition + 1;
        } else {
            currentVerticalPosition = currentVerticalPosition + emptyPlaces;
        }

        // Check if the new vertical position doesn't exceed the number of max products that can be in a row
        // If yes, then we know he can fly to the next row (taking into account a bit of extra space with the height of the planks)
        if (currentVerticalPosition <= maxProductsVerticalPerRow) {
            // FLY + SCAN
            double milimetersToFlyUp = product.getHeight() * 10 * currentVerticalPosition;

            cmd.up(speed * 2).doFor((long) (milimetersToFlyUp * timeToFlyOneMiliMeter));
            cmd.hover().doFor(2000);
            scanProduct();
        } else {
            currentRow++;

            // Check if he has to fly to the next row
            if (currentRow <= shelf.getNumberOfRows()) {
                // FLY + SCAN
                double milimetersToFlyUp = (product.getHeight() * 10 * currentVerticalPosition) + shelf.getShelfSeparatorHeight() * 10;
                cmd.up(speed * 3).doFor((long) (milimetersToFlyUp * timeToFlyOneMiliMeter));
                cmd.hover().doFor(2000);
                scanProduct();
            } else {
                currentHorizontalPosition++;

                // Check if he has to fly to the right
                if (currentHorizontalPosition <= maxProductsHorizontalPerShelf) {
                    // TO THE RIGHT
                    double milimetersToGoRight = (product.getWidth() * 10);
                    cmd.goRight(speed / 3).doFor((long) (milimetersToGoRight * timeToFlyOneMiliMeter));
                    cmd.hover().doFor(2000);

                    // Completely down
                    double milimetersToFlyDown = (shelf.getShelfHeight() * 10) - ((product.getHeight() * 10) / 2);
                    cmd.down(speed * 5).doFor((long) (milimetersToFlyDown * timeToFlyOneMiliMeter) * 2);
                    cmd.hover().doFor(2000);
                    currentRow = 1;

                    scanProduct();
                } else {
                    // We are done counting this shelf!
                    // Continue with the route
                    QRCodeScanner.getInstance().removeObserver(this);
                    cmd.setVideoChannel(VideoChannel.VERT);

                    followRouteController.continueRoute();
                }
            }
        }
    }

    /**
     * Try to scan the product, within 3 seconds
     * - If succesfull, repeat step 1-2
     * - If not succesfull, skip all the packages till next row
     */
    private void scanProduct() {
        QRCodeScanner.getInstance().registerObserver(this);

        // Timer 3 seconds, after that remove the listener and check for productWasScanned
        scheduledFuture = Executors.newSingleThreadScheduledExecutor().schedule(new Runnable() {
            @Override
            public void run() {
                checkIfProductWasScanned();
            }
        }, secondsToScanTheProduct, TimeUnit.SECONDS);
    }

    private void stopTimer() {
        scheduledFuture.cancel(false);
        checkIfProductWasScanned();
    }

    private void checkIfProductWasScanned() {
        QRCodeScanner.getInstance().removeObserver(this);
        if (productWasScanned) {
            flyToNextProduct(0);
            productWasScanned = false;
        } else {
            // Calculate the empty places and skip them
            int emptyPlaces = (int) (maxProductsVerticalPerRow - currentVerticalPosition + 1); // +1 to count the current position also as empty place
            totalAmountOfEmptyPlaces += emptyPlaces;
            flyToNextProduct(emptyPlaces);
        }
    }

    /**
     * Method that attempts to center the drone in front of a detected QR code
     * x y values correspond with the rectangle of the grid the QR code was found in
     *
     */

    @Override
    public void QRGridUpdate(int x, int y) {
        switch (x) {
            case 0:
                cmd.goLeft(speed / 3);
                break;
            case 1:
                break;
            case 2:
                cmd.goRight(speed / 3);
                break;
        }
        switch (y) {
            case 0:
                cmd.up(speed);
                break;
            case 1:
                break;
            case 2:
                cmd.down(speed);
                break;
        }
    }

}
