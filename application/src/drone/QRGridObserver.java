package drone;

/**
 * Created by Robert on 16-Jan-17.
 */
public interface QRGridObserver extends Observer {
    public void QRGridUpdate(int x, int y);

}
