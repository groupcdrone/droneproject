/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.routes;

import drone.Controller;
import drone.routes.RoutesView;

/**
 *
 * @author erikj
 */
public class RoutesController extends Controller {

    private static RoutesController ins;

    private RoutesView routeView;

    public static RoutesController getInstance(RoutesView rv) {
        if (ins == null) {
            ins = new RoutesController(rv);
        }
        return ins;

    }

    private RoutesController(RoutesView rv) {
        super(rv);
        routeView = rv;
    }

}
