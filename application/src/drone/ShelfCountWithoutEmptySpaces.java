/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone;

import com.google.zxing.Result;
import de.yadrone.base.command.CommandManager;
import api.APIHandler;
import de.yadrone.base.command.VideoChannel;
import drone.follow_route.FollowRouteController;
import drone.model.Drone;
import drone.model.Product;
import drone.model.Shelf;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Mike
 */
public class ShelfCountWithoutEmptySpaces implements ShelfCountBehavior {

    private FollowRouteController followRouteController;
    private final ArrayList<Result> scannedResults;

    private boolean shelfCodeWasScanned;
    private ScheduledFuture scheduledCommand;
    private final int scheduledCommandTime = 100; // ms

    private ScheduledFuture scheduledMaxTimeCounter;
    private final int scheduledMaxTimeCounterTime = 4000; // ms

    private Shelf shelf;
    private Product product;

    private double maxProductsVerticalPerShelf;
    private double maxProductsHorizontalPerShelf;

    private int currentYPosition;
    private int currentXPosition;

    private final CommandManager cmd;
    private final int speed = 2; // percentage of max speed

    public ShelfCountWithoutEmptySpaces() {
        scannedResults = new ArrayList<>();
        cmd = Drone.getInstance().getCommandManager();
        cmd.setVideoChannel(VideoChannel.VERT);

        shelfCodeWasScanned = false;

        currentXPosition = 1;
        currentYPosition = 1;
    }

    public void setFollowRouteController(FollowRouteController fc) {
        followRouteController = fc;
    }

    @Override
    public void QRGridUpdate(int x, int y) {
        switch (x) {
            case 0:
                cmd.goLeft(speed / 3);
                break;
            case 1:
                break;
            case 2:
                cmd.goRight(speed / 3);
                break;
        }
        switch (y) {
            case 0:
                cmd.up(speed);
                break;
            case 1:
                break;
            case 2:
                cmd.down(speed);
                break;
        }
    }

    @Override
    public void onTag(Result result, float f) {
        if (result != null) {
            boolean exists = false;

            for (Result r : scannedResults) {
                if (r.getText().equals(result.getText())) {
                    exists = true;
                }
            }
            if (!exists) {
                scannedResults.add(result);

                // First time the function gets here he should've scanned a Shelf QR-code
                if (!shelfCodeWasScanned) {
                    shelfCodeWasScanned = true;
                    cmd.setVideoChannel(VideoChannel.HORI);

                    getProductInfoFromShelf(result);
                } else {
                    // Else it should be a product
                    scheduledCommand.cancel(false);
                    scheduledMaxTimeCounter.cancel(true);
                    cmd.hover().doFor(1000);
                    cmd.backward(1).doFor(500);
                    cmd.hover();

                    if (currentXPosition % 2 == 1 && currentYPosition < maxProductsVerticalPerShelf) {
                        currentYPosition++;

                        startMaxTimeCounter();
                        scheduledCommand = Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new Runnable() {
                            @Override
                            public void run() {
                                cmd.up(speed);
                            }
                        }, 0, scheduledCommandTime, TimeUnit.MILLISECONDS);
                        cmd.hover();
                    } else if (currentYPosition == maxProductsVerticalPerShelf && currentXPosition < maxProductsHorizontalPerShelf) {
                        currentXPosition++;
                        currentYPosition = 1;

                        startMaxTimeCounter();
                        scheduledCommand = Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new Runnable() {
                            @Override
                            public void run() {
                                cmd.goRight(1);
                            }
                        }, 0, scheduledCommandTime, TimeUnit.MILLISECONDS);
                        cmd.hover();
                    } else if (currentXPosition % 2 == 0 && currentYPosition < maxProductsVerticalPerShelf) {
                        currentYPosition++;

                        startMaxTimeCounter();
                        scheduledCommand = Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new Runnable() {
                            @Override
                            public void run() {
                                cmd.down(speed);
                            }
                        }, 0, scheduledCommandTime, TimeUnit.MILLISECONDS);
                        cmd.hover();
                    } else if (currentXPosition >= maxProductsHorizontalPerShelf) {
                        // We are done counting with this shelf!
                        followRouteController.continueRoute();
                    }
                }
            }
        }
    }

    private void getProductInfoFromShelf(Result result) {
        APIHandler api = APIHandler.getInstance();
        shelf = api.getShelfInfo(result.getText());
        product = api.getProductInforById(shelf.getProductID());

        double heightOfRow = (shelf.getShelfHeight() - ((shelf.getNumberOfRows() - 1) * shelf.getShelfSeparatorHeight())) / shelf.getNumberOfRows();
        double maxProductsVerticalPerRow = Math.floor(heightOfRow / product.getHeight());
        maxProductsVerticalPerShelf = maxProductsVerticalPerRow * shelf.getNumberOfRows();
        maxProductsHorizontalPerShelf = Math.floor(shelf.getShelfWidth() / product.getWidth());
    }

    private void startMaxTimeCounter() {
        scheduledMaxTimeCounter = Executors.newSingleThreadScheduledExecutor().schedule(new Runnable() {
            @Override
            public void run() {
                if (!scheduledCommand.isCancelled()) {
                    scheduledCommand.cancel(true);
                    // Error!
                }
            }
        }, scheduledMaxTimeCounterTime, TimeUnit.MILLISECONDS);
    }

    @Override
    public void countShelf() {
        QRCodeScanner.getInstance().registerObserver(this);

        // Initially the drone needs to fly up to find the first package
        cmd.backward(2).doFor(500);
        cmd.hover();

        startMaxTimeCounter();
        scheduledCommand = Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                cmd.up(speed);
            }
        }, 0, scheduledCommandTime, TimeUnit.MILLISECONDS);
        cmd.hover();
    }
}
