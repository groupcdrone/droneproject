package drone;

/**
 * Created by Robert on 12-Jan-17.
 */
public interface VideoObserver extends Observer {
    public void gridUpdate(int x, int y);
}
