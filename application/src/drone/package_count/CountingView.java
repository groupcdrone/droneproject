/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.package_count;

import drone.model.ScannedItem;
import drone.View;

/**
 *
 * @author Mike
 */
public interface CountingView extends View {
    public void addNewItem(ScannedItem item);
    public void updateCountForItem(ScannedItem item);
}
