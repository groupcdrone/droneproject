/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.package_count;

import api.APIHandler;
import drone.model.Product;
import drone.model.ScannedItem;
import javax.swing.JLabel;

/**
 *
 * @author Mike
 */
public class CountedItem extends javax.swing.JPanel {

    private ScannedItem scannedItem;

    public ScannedItem getScannedItem() {
        return scannedItem;
    }

    /**
     * Creates new form CountedItem
     */
    public CountedItem(ScannedItem item) {
        initComponents();
        scannedItem = item;

        APIHandler api = APIHandler.getInstance();
        Product p = api.getProductInfo(item.getResult().getText());

        productLabel.setText(p.getName());
        amountLabel.setText("1");
    }
    
    public CountedItem() {
        initComponents();       
        amountLabel.setText("1");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        productLabel = new javax.swing.JLabel();
        amountLabel = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 164, 148));
        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 150, 135)));
        setMaximumSize(new java.awt.Dimension(240, 32));
        setMinimumSize(new java.awt.Dimension(240, 32));
        setPreferredSize(new java.awt.Dimension(240, 32));
        setLayout(new java.awt.GridBagLayout());

        productLabel.setFont(new java.awt.Font("Calibri", 0, 16)); // NOI18N
        productLabel.setForeground(new java.awt.Color(255, 255, 255));
        productLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        productLabel.setText("Productname");
        productLabel.setMaximumSize(new java.awt.Dimension(198, 30));
        productLabel.setMinimumSize(new java.awt.Dimension(198, 30));
        productLabel.setPreferredSize(new java.awt.Dimension(198, 30));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        add(productLabel, gridBagConstraints);

        amountLabel.setBackground(new java.awt.Color(0, 117, 106));
        amountLabel.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        amountLabel.setForeground(new java.awt.Color(255, 255, 255));
        amountLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        amountLabel.setText("0");
        amountLabel.setMaximumSize(new java.awt.Dimension(20, 20));
        amountLabel.setMinimumSize(new java.awt.Dimension(20, 20));
        amountLabel.setOpaque(true);
        amountLabel.setPreferredSize(new java.awt.Dimension(30, 30));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(amountLabel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel amountLabel;
    private javax.swing.JLabel productLabel;
    // End of variables declaration//GEN-END:variables

    public JLabel getAmountLabel() {
        return amountLabel;
    }

    public void updateCountLabel() {
        amountLabel.setText(Integer.toString(Integer.parseInt(amountLabel.getText()) + 1));
    }
}
