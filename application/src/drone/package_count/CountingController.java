package drone.package_count;

import com.google.zxing.Result;
import drone.model.ScannedItem;
import drone.QRCodeScanner;
import drone.Controller;
import drone.TagObserver;

import java.util.ArrayList;

/**
 * Created by Robert on 09-Dec-16.
 */
public class CountingController extends Controller implements TagObserver {
    
    private final CountingView countingView;
    
    private final ArrayList<ScannedItem> scannedItems = new ArrayList<>();
    private long lastResultStamp = System.currentTimeMillis();
    
    public CountingController(CountingView cv) {
        super(cv);
        countingView = cv;
        
        QRCodeScanner.getInstance().registerObserver(this);
    }


    /**
     * Method that gets called on every frame that has a QR code
     * Adds items to arraylist based on whether or not the product has been scanned before
     * Only adds an item every 5 seconds as  to avoid duplicate entries of the same product
     */
    @Override
    public void onTag(Result result, float v) {
        if (result != null && System.currentTimeMillis() - lastResultStamp > 5000) {
            lastResultStamp = System.currentTimeMillis();

            boolean exists = false;

            for (ScannedItem s : scannedItems) {
                if (s.getResult().getText().equals(result.getText())) {
                    exists = true;
                    s.setCount(s.getCount() + 1);
                    countingView.updateCountForItem(s);
                }
            }
            if (!exists) {
                scannedItems.add(new ScannedItem(result, 1));
                countingView.addNewItem(new ScannedItem(result, 1));
            }
        }
    }
}
