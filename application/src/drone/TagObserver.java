/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone;

import com.google.zxing.Result;

/**
 *
 * @author Mike
 */
public interface TagObserver extends Observer {
    public void onTag(Result result, float f);
}
