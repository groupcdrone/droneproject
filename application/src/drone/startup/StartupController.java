/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.startup;

import drone.Controller;

/**
 *
 * @author Mike
 */
public final class StartupController extends Controller {

    private static StartupController ins;

    private final StartupView startupView;
    
    public static StartupController getInstance(StartupView sv) {
        if(ins == null) {
            ins = new StartupController(sv);
        }
        return ins;
    }
    
    private StartupController(StartupView sv) {
        super(sv);
        startupView = sv;
    }
}
