/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.main_menu;

import drone.View;

/**
 *
 * @author Mike
 */
public interface MainView extends View {
    
    public void showPane();
    
    public void setDroneConnectedBtn(boolean connected);

    public void initializeAllPanes();
}
