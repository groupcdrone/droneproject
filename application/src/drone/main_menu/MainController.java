/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone.main_menu;

import drone.Controller;
import drone.model.Drone;

/**
 *
 * @author Mike
 */
public final class MainController extends Controller {
    
    private static MainController ins;
    
    private final MainView mainView;
    private final Drone drone;
    private boolean connected;

    public static MainController getInstance(MainView mv) {
        if(ins == null) {
            ins = new MainController(mv);
        }
        return ins;
    }
    
    private MainController(MainView mv) {
        super(mv);
        mainView = mv;

        connected = false;
        drone = Drone.getInstance(); // Here we make the drone, at this point there is no instance of Drone

        setDroneConnected();
        showPane();
    }
    
    private void showPane() {
        mainView.showPane();
    }

    @Override
    public void showNextPane(String pane) {
        if (pane == "STARTUP" && !connected) {
            // Do nothing
            mainView.showNextPane(pane); // Only for testing purposes!
        } else {
            mainView.showNextPane(pane);
        }
    }

    private void setDroneConnected() {
        boolean commandManagerReady = drone.getCommandManager().isConnected();
        boolean navDataReady = drone.getNavDataManager().isConnected();
        boolean videoManagerReady = drone.getVideoManager().isConnected();

        // System.out.println("" + commandManagerReady + navDataReady + videoManagerReady);

        // This currently only works at the first startup of the application!
        if (commandManagerReady && navDataReady && videoManagerReady) {
            connected = true;
        } else {
            connected = true;
        //    drone.getIARDrone().stop();
        }
        
        mainView.setDroneConnectedBtn(connected);
    }
}
