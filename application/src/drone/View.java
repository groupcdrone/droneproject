/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drone;

/**
 *
 * @author Mike
 */
public interface View {

    public void setController(Controller controller);

    public void showNextPane(String pane);
}
