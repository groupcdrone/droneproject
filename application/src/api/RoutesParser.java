/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import api.Parser;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import drone.model.Route;
import java.util.ArrayList;

/**
 *
 * @author erikj
 */
public class RoutesParser extends Parser{

    @Override
    public Object getObject(String json) {
        
        ArrayList<Route> routeList = new ArrayList();
        
        JsonElement jelement = new JsonParser().parse(json);
            JsonObject jobject = jelement.getAsJsonObject();
            JsonArray arr = jobject.getAsJsonArray("routes");
            JsonObject ooo = arr.get(0).getAsJsonObject();
            String error = jobject.get("error").toString();

            if (!Boolean.valueOf(error)) {
                for (int i = 0; i < arr.size(); i++) {
                    JsonObject route = arr.get(i).getAsJsonObject();
                    route.toString();
                    Gson gson = new Gson();
                    Route aRoute = gson.fromJson(route.toString(), Route.class);
                    routeList.add(aRoute);
                }
            }
            
            return routeList;
    }
    
}
