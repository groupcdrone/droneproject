/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import api.Parser;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import drone.model.Shelf;

/**
 *
 * @author erikj
 */
public class ShelfParser extends Parser {

    @Override
    public Object getObject(String json) {
        Shelf shelf = null;
        JsonElement jelement = new JsonParser().parse(json);
        JsonObject jobject = jelement.getAsJsonObject();
        String error = jobject.get("error").toString();

        if (!Boolean.valueOf(error)) {
            Gson gson = new Gson();
            shelf = gson.fromJson(jobject.toString(), Shelf.class);
        }
        return shelf;
    }

}
