/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import drone.model.FlightInstruction;
import java.util.ArrayList;

/**
 *
 * @author erikj
 */
public class FlightInstructionsParser extends Parser {

    @Override
    public Object getObject(String json) {
        ArrayList<FlightInstruction> flightInstructionList = new ArrayList();

        JsonElement jelement = new JsonParser().parse(json);
        JsonObject jobject = jelement.getAsJsonObject();
        JsonArray arr = jobject.getAsJsonArray("routes");
        String error = jobject.get("error").toString();

        if (!Boolean.valueOf(error)) {
            for (int i = 0; i < arr.size(); i++) {
                JsonObject instruction = arr.get(i).getAsJsonObject();
                instruction.toString();
                Gson gson = new Gson();
                FlightInstruction anInstruction = gson.fromJson(instruction.toString(), FlightInstruction.class);
                flightInstructionList.add(anInstruction);
            }
        }

        return flightInstructionList;
    }

}
