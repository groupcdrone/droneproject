/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import com.google.gson.Gson;

/**
 *
 * @author erikj
 * @param <AnyType>
 */
public abstract class Parser<AnyType> {
    
    // public IJsonParser jsonparser;
    
    public Parser(){       
    }
    
    public String toJson(AnyType object){
        Gson gson = new Gson();
        return gson.toJson(object);
    }
    
    public abstract AnyType getObject(String json);  

}
