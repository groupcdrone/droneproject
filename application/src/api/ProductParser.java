/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import api.Parser;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import drone.model.Product;
import drone.model.Shelf;

/**
 *
 * @author erikj
 */
public class ProductParser extends Parser{

    @Override
    public Object getObject(String json) {
        Product product      = null;
        JsonElement jelement = new JsonParser().parse(json);
        
        JsonObject jobject   = jelement.getAsJsonObject();
        String error         = jobject.get("error").toString();

        
         if (!Boolean.valueOf(error)) {
            Gson gson = new Gson();
            product   = gson.fromJson(jobject.toString(), Product.class);
        }
              
        
        return product;
    }

  
   
    
    
}
