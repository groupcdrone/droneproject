/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import drone.model.FlightInstruction;
import drone.model.Route;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;
import java.net.HttpURLConnection;
import drone.model.Product;
import drone.model.Shelf;
import java.io.InputStreamReader;

/**
 *
 * @author erikj
 */
public class APIHandler {
    
    private static APIHandler instance;

    private static final String defaultUrl            = "http://localhost:8080/drone_api/v1/routes";
    private static final String routeUrl              = "http://localhost:8080/drone_api/v1/routes";
    private static final String productInfoUrl        = "http://localhost:8080/drone_api/v1/product_info/";
    private static final String productInfoByIDUrl    = "http://localhost:8080/drone_api/v1/product_by_id/";
    private static final String flightInstructionsUrl = "http://localhost:8080/drone_api/v1/flight_instruction/";
    private static final String shelfUrl              = "http://localhost:8080/drone_api/v1/shelf_info/";

    
    private Scanner scanner;
    private Parser parser; 
    private final String charset = "UTF-8";  // Or in Java 7 and later, use the constant: java.nio.charset.StandardCharsets.UTF_8.name()
  

    private APIHandler() {

    }
    
    public static APIHandler getInstance(){
        if(instance == null){
            instance = new APIHandler();
        }
        
        return instance;
    }

    public boolean hasConnection() {
        boolean hasConnection;
        try {
            InputStreamReader read = new InputStreamReader(getJsonResponse(routeUrl));
            hasConnection = true;

        } catch (NullPointerException np) {
            hasConnection = false;
            System.out.println("No connection with server");
        }
        System.out.println("bool : " + hasConnection);
        return hasConnection;
    }

    public ArrayList<Route> getRoutes() {
        ArrayList<Route> routeList = null;

        try {
            scanner = new Scanner(getJsonResponse(routeUrl));
            String response = scanner.useDelimiter("\\A").next();
            System.out.println(response);

            parser = new RoutesParser();
            routeList = (ArrayList<Route>)parser.getObject(response);
 
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());

        }

        return routeList;
    }

    public ArrayList<FlightInstruction> getFlightInstructions(int routeID) {
        ArrayList<FlightInstruction> flightInstructionList = null;

        try {
            scanner = new Scanner(getJsonResponse(flightInstructionsUrl + routeID));
            String response = scanner.useDelimiter("\\A").next();
            System.out.println(response);
 
            parser = new FlightInstructionsParser();
            flightInstructionList = (ArrayList<FlightInstruction>)parser.getObject(response);        
          

        } catch (Exception e) {
            e.printStackTrace();
        }

        return flightInstructionList;
    }
    
    public Product getProductInfo(String productCode){
        Product product = null;
        try{
            scanner = new Scanner(getJsonResponse(productInfoUrl + productCode));  
            String response = scanner.useDelimiter("\\A").next();            
            parser = new ProductParser();
            product = (Product)parser.getObject(response);           
            
        }catch (Exception e){
            e.printStackTrace();
        }
        
        return product;
    }
    
    public Product getProductInforById(int id){
        Product product = null;
        try{
            scanner = new Scanner(getJsonResponse(productInfoByIDUrl + id));
            String response = scanner.useDelimiter("\\A").next();
            parser = new ProductParser();
            product = (Product)parser.getObject(response);
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return product;
    }
    
 
    public Shelf getShelfInfo(String shelfCode){
        Shelf shelf = null;
        try{
            scanner = new Scanner(getJsonResponse(shelfUrl + shelfCode));
            String response = scanner.useDelimiter("\\A").next();
            
            parser = new ShelfParser();
            shelf = (Shelf)parser.getObject(response);            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return shelf;
    }

    private InputStream getJsonResponse(String urlToOpen) {
        InputStream inputStream;
        try {
            URL url = new URL(urlToOpen);
            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            switch (httpConn.getResponseCode()) {
                case 200:
                    inputStream = httpConn.getInputStream();
                    break;
                case 404:
                    inputStream = httpConn.getErrorStream();
                    break;
                default:
                    inputStream = null;
                    break;
            }
        } catch (Exception e) {
            System.out.println(e.toString());
            inputStream = null;
        }
        return inputStream;
    }

}
