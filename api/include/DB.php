<?php
class DB
{
    public static function GetConnection()
    {

        define('USER', 'root');
        define('PASSWORD', 'usbw');
        define('HOST', 'localhost');
        define('NAME', 'DroneDB');
        define('PORT', '3307');

        $dbh = new PDO(
            'mysql:host=' . HOST . ';port=' . PORT . ';dbname=' . NAME,
            USER, PASSWORD);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        return $dbh;
    }

}
?>