<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Erik-Jan Westendorp
 *
 */
class DbHandler {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        include 'DB.php';
        $db = new DbConnect();
        $this->conn = $db->connect();
        $this->dbh = DB::GetConnection();
    }

    /**
     * Fetching single product
     * @param $product_code
     * @return array|null
     * @internal param $product_id
     */
    public function getProductInfo($product_code) {

        $stmt = $this->dbh->prepare("SELECT * FROM product WHERE ProductCode = :id LIMIT 1");
        $stmt->bindValue(":id", $product_code, PDO::PARAM_STR);
        if ($stmt->execute()) {
            $row = $stmt->fetch();
            return $row;
        } else {
            return NULL;
        }

    }

    /**
     * Fetching product information
     * @param $product_id
     * @return mixed|null
     */
    public function getProductInfoById($product_id){
        $stmt =  $this->dbh->prepare("SELECT * FROM product where ProductID = :id LIMIT 1");
        $stmt->bindValue(":id", $product_id);
        if($stmt->execute()){
            $row = $stmt->fetch();
            return $row;
        }else{
            return NULL;
        }
    }

    /**
     * Fetching information of a shelf
     * @param $shelf_code
     * @return mixed|null
     */
    public function getShelfInfo($shelf_code){
        $stmt = $this->dbh->prepare("SELECT * FROM shelf WHERE ShelfCode = :shelf_code LIMIT 1");
        $stmt->bindValue(":shelf_code", $shelf_code);
        if($stmt->execute()){
            $row = $stmt->fetch();
            return $row;
        }else{
            return NULL;
        }
    }


    /**
     * Fetching al list of routes
     * @return array|null
     */
    public function getRoutes(){
        $stmt = $this->dbh->prepare("SELECT * FROM route");
        if($stmt->execute()){
            return $stmt->fetchAll();
        }else{
            return NULL;
        }

    }

    /**
     * Fetching a list with flight instructions
     * @param $route_id
     * @return array|null
     */
    public function getFlightInstructions($route_id){
        $stmt = $this->dbh->prepare("SELECT * FROM flightinstruction WHERE RouteID = :RouteID ORDER BY InstructionNumber ");
        $stmt->bindValue("RouteID", $route_id);
        if($stmt->execute()){
            return $records = $stmt->fetchAll();
        }else{
            return NULL;
        }
    }

    /**
     * Updates the amount of a product
     * @param $product_code
     * @param $amount
     * @return bool
     */
    public function updateProductAmount($product_code, $amount){
        $stmt = $this->dbh->prepare("UPDATE product SET Amount = :amount WHERE ProductCode = :product_code");
        $stmt->bindValue("product_code", $product_code);
        $stmt->bindValue("amount", $amount);
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }

    /**
     * A simple method that checks if a product exists
     * @param $product_code
     * @return bool
     */
    public function checkIfProductExist($product_code){
        $stmt = $this->dbh->prepare("SELECT ProductCode FROM product WHERE ProductCode = :product_code LIMIT 1");
        $stmt->bindValue("product_code", $product_code);
        $stmt->execute();

        $count = $stmt->rowCount();

        if($count == 1){
            return true;
        }else{
            return false;
        }
    }


}

?>
