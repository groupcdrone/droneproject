<?php

require_once '../include/DbHandler.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();


$user_id = NULL;


/**
 * Returns product info if exist
 * method GET
 * url /product_info/:id
 * Will return 404 if product not exists
 */
$app->get('/product_info/:id', function($location_id) {

        $response = array();
        $db = new DbHandler();

        // fetch task
        $result = $db->getProductInfo($location_id);

        if ($result["ProductID"] != NULL) {
            $response["error"]          = false;
            $response["ProductID"]      = $result["ProductID"];
            $response["ProductCode"]    = $result["ProductCode"];
            $response["ProductName"]    = $result["ProductName"];
            $response["ProductHeight"]  = $result["ProductHeight"];
            $response["ProductWidth"]   = $result["ProductWidth"];
            $response["Amount"]         = $result["Amount"];
            echoRespnse(200, $response);
        } else {
            $response["error"] = true;
            $response["message"] = "The requested product doesn't exists";
            echoRespnse(404, $response);
        }
    });

/**
 * This method will return the product information where is asked for
 * method GET
 * url /product_by_id:id
 * Will return 404 if product not exist
 */
$app->get('/product_by_id/:id', function($product_id){
   $response = array();
   $db       = new DbHandler();

   // fetch task
   $result = $db->getProductInfoById($product_id);

   if($result["ProductID"] != NULL){
       $response["error"]          = false;
       $response["ProductID"]      = $result["ProductID"];
       $response["ProductCode"]    = $result["ProductCode"];
       $response["ProductName"]    = $result["ProductName"];
       $response["ProductHeight"]  = $result["ProductHeight"];
       $response["ProductWidth"]   = $result["ProductWidth"];
       $response["Amount"]         = $result["Amount"];
       echoRespnse(200, $response);
   }else{
       $response["error"]   = true;
       $response["message"] = "The requested product doesn't exists";
       echoRespnse(404, $response);
   }

});


/**
 *  Returns shelf info
 *  method GET
 *  url /shelf_info/:id
 *  Will return 404 when result is null
 */
$app->get('/shelf_info/:id', function($shelf_code){

    $response = array();
    $db = new DbHandler();

    $result = $db->getShelfInfo($shelf_code);

    if ($result["ShelfID"] != NULL) {
        $response["error"]                = false;
        $response["ShelfID"]              = $result["ShelfID"];
        $response["ShelfCode"]            = $result["ShelfCode"];
        $response["ProductID"]            = $result["ProductID"];
        $response["ShelfHeight"]          = $result["ShelfHeight"];
        $response["ShelfWidth"]           = $result["ShelfWidth"];
        $response["ShelfSeparatorHeight"] = $result["ShelfSeparatorHeight"];
        $response["NumberOfRows"]         = $result["NumberOfRows"];

        echoRespnse(200, $response);
    } else {
        $response["error"] = true;
        $response["message"] = "The requested shelf doesn't exists";
        echoRespnse(404, $response);
    }

});

/**
 * Listing all routes
 * method GET
 * url /routes
 * returns 404 if no route found
 */
$app->get('/routes',  function() {
    $response = array();
    $db = new DbHandler();
    $result = $db->getRoutes();


    if($result != NULL){
        $route_list = array();
        foreach ($result as $row => $record) {
            $route_list[] = array(
                "RouteID"  => $record["RouteID"],
                "Name"           => $record["Name"],
                "Description" => $record["Description"]
            );
        }

        $response["error"] = false;
        $response["routes"] = $route_list;
        echoRespnse(200, $response);
    }else{
        $response["error"] = true;
        $response["message"] = "No routes found";
        echoRespnse(404, $response);
    }

});

/**
 *  Returns list of flight instruction
 *  method GET
 *  url /flight_instruction/:route_id
 *  Will return 404 when result is null
 */
$app->get('/flight_instruction/:route_id', function($route_id){

    $response = array();
    $db = new DbHandler();

    $result = $db->getFlightInstructions($route_id);


    if($result != NULL){
        $instruction_list = array();
        foreach ($result as $row => $record) {
            $instruction_list[] = array(
                "FlightInstructID"  => $record["FlightInstructID"],
                "RouteID"           => $record["RouteID"],
                "InstructionNumber" => $record["InstructionNumber"],
                "InstructionCode"   => $record["InstructionCode"],
                "Description"       => $record["Description"],
            );
        }

        $response["error"] = false;
        $response["routes"] = $instruction_list;

        echoRespnse(200, $response);
    }else{
        $response["error"] = true;
        $response["message"] = "No flight instructions found";
        echoRespnse(404, $response);
    }

});

/**
 *  Updates amount of given product by id
 *  method POST
 *  url /update_product_amount
 *  Will return 400 when something went wrong
 */
$app->post('/update_product_amount', function() use ($app){
    //check for required parameters
    verifyRequiredParams(array('product_code','amount'));

    //Read the post parameters
    $product_code = $app->request()->post('product_code');
    $amount       = $app->request()->post('amount');

    $db = new DbHandler();

    $exist = $db->checkIfProductExist($product_code);


    if($exist && $db->updateProductAmount($product_code, $amount) && is_numeric($amount)){
        $response = array();
        $response["error"] = false;
        $response["message"] = "Amount has been updated";
        echoRespnse(200, $response);
    }else{
        $response = array();
        $response["error"] = true;
        $response["message"] = "Something went wrong";
        echoRespnse(400, $response);
    }
});

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>